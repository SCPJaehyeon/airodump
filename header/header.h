#pragma once
#include <iostream>
#include <cstdio>
#include <stdint.h>
#include <arpa/inet.h>
#include <cstring>
#include <pcap/pcap.h>
#include <map>
#include <unistd.h>
#include <cstdlib>
#define MAC_LEN 6
#define TYPE    24
#define PWR 18
#define BSSID   40
#define BEACON_LEN  61
#define BEACON_SSID 62
#define PROBE_LEN   49
#define PROBE_SSID  50
#define ReqSTATION 34
#define ResSTATION 28

class Mac{
protected:
    u_char mac[MAC_LEN];
public:
    Mac() {};
    ~Mac() {};
    operator u_char*() const{
        return (u_char*)mac;
    }
};

struct mac_s {
    Mac mac_addr;
    bool operator<(const mac_s& omac_s) const{
        return memcmp(this->mac_addr, omac_s.mac_addr, MAC_LEN)<0;
    }
};

struct beaconinfo{
    char pwr;
    u_int8_t beacons;
    u_int8_t Chanel;
    std::string essid;
};

struct probeinfo{
    Mac bssidmac;
    char pwr;
    u_int8_t frame;
    std::string probe;

};

int cmp_beacon(const u_char *cmp);
int cmp_probereq(const u_char *cmp);
int cmp_proberes(const u_char *cmp);
int show_airodump(char *dev, int channel);
void Usage(char *argv[]);
void print_mac(Mac m);
