#include "header/header.h"
using namespace std;

void Usage(char* argv[]){
    printf("%s [device] [-c channel]\n", argv[0]);
    printf("ex:) %s wlan0\n",argv[0]);
    printf("ex:) %s wlan0 -c 2\n",argv[0]);
}

void print_mac(Mac m){ //Print Source MAC, Destination MAC
    printf("%02x:%02x:%02x:%02x:%02x:%02x \t : \t",m[0],m[1],m[2],m[3],m[4],m[5]);
}
