#include "header/header.h"
using namespace std;

int show_airodump(char *dev, int channel){
    //PCAP
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf); // if file : pcap_open_offline
    if (handle == NULL) {
      fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
      return -1;
    }
    struct pcap_pkthdr* header;
    const u_char* packet;
    int res = pcap_next_ex(handle, &header, &packet);

    //Channel Filter
    bool channel_fix = false;
    bool exist_beacon = true;
    bool exist_probereq = true;
    bool exist_proberes = true;
    bool try_add = false;
    if(channel == 0){
        channel_fix = false;
    }else if(channel > 0){
        channel_fix = true;
    }else{
        printf("Channel designation error \n");
        return -1;
    }

    //Var
    Mac char_beacon_bssid, char_probe_station, char_probe_bssid; //beacon, probe init
    u_int i=1, j=1 ,essid_length=0, probe_length=0;

    u_int cnt_beacon = 0, pwr_beacon=0, chanel_beacon=0; //beacon
    string essid_beacon = "";

    u_int cnt_frame = 0, cnt_frame2 = 0, pwr_probe = 0, pwr_probe2 = 0; //probe
    string essid_probe = "", essid_probe2 = "";

    Mac broad;
    memset(broad, 0xff, MAC_LEN);

    //Struct
    struct mac_s bssid_s, station_s;
    struct beaconinfo beaconinfo_s;
    struct probeinfo probeinfo_s;

    //Map<struct, struct>
    map<mac_s, beaconinfo> beacon_info;
    map<mac_s, beaconinfo>::iterator beacon_infoit;
    map<mac_s, probeinfo> probe_info;
    map<mac_s, probeinfo>::iterator probe_infoit;

    while(res != -1 || res != -2){
        if(cmp_beacon(&packet[TYPE])==1 || cmp_probereq(&packet[TYPE])==1 || cmp_proberes(&packet[TYPE])==1){
            system("clear");
            printf("CH%d] Information \n", channel);
            i = 1, j = 1;
            if(cmp_beacon(&packet[TYPE])==1){ //BEACON FRAME
                memcpy(&bssid_s.mac_addr[0], &packet[BSSID], MAC_LEN); //ap bssid
                memcpy(&beaconinfo_s.pwr, &packet[PWR], 1); //ap pwr
                memcpy(&essid_length, &packet[BEACON_LEN],1);
                beaconinfo_s.essid.resize(essid_length);
                memcpy((char*)beaconinfo_s.essid.data(), &packet[BEACON_SSID],essid_length); //ap essid
                memcpy(&beaconinfo_s.Chanel, &packet[BEACON_LEN+essid_length+13],1); //ap channel
                if(channel_fix == false){
                    auto ret = beacon_info.insert(make_pair(bssid_s,beaconinfo_s));
                    exist_beacon = ret.second;
                    try_add = true;
                }else if(channel_fix == true && int(packet[BEACON_LEN+essid_length+13]) == channel){
                    auto ret = beacon_info.insert(make_pair(bssid_s,beaconinfo_s));
                    exist_beacon = ret.second;
                    try_add = true;
                }else{
                    try_add = false;
                }
                if(exist_beacon == true && try_add == true) { //if new keyw
                    memcpy(&beaconinfo_s.pwr, &packet[PWR], 1);
                    memcpy(&essid_length, &packet[BEACON_LEN],1);
                    beaconinfo_s.essid.resize(essid_length);
                    memcpy((char*)beaconinfo_s.essid.data(), &packet[BEACON_SSID],essid_length);
                    memcpy(&beaconinfo_s.Chanel, &packet[BEACON_LEN+essid_length+13],1);
                    cnt_beacon = 1;
                    beaconinfo_s.beacons = cnt_beacon;
                    beacon_info[bssid_s] = beaconinfo_s;
                }else if(exist_beacon == false && try_add == true){ //if exist
                    pwr_beacon = beacon_info.find(bssid_s)->second.pwr;
                    essid_beacon = beacon_info.find(bssid_s)->second.essid.c_str();
                    chanel_beacon = beacon_info.find(bssid_s)->second.Chanel;
                    cnt_beacon = beacon_info.find(bssid_s)->second.beacons + 1; //beacons count +1
                    beaconinfo_s.beacons = pwr_beacon;
                    beaconinfo_s.essid = essid_beacon;
                    beaconinfo_s.Chanel = chanel_beacon;
                    beaconinfo_s.beacons = cnt_beacon;
                    beacon_info[bssid_s] = beaconinfo_s;
                }
            }
            else if(cmp_probereq(&packet[TYPE])==1){ //PROBE REQUEST
                memcpy(&station_s.mac_addr[0], &packet[ReqSTATION], MAC_LEN); //station
                memcpy(&probeinfo_s.bssidmac[0], &packet[BSSID], MAC_LEN);
                memcpy(&probeinfo_s.pwr, &packet[PWR],1);
                memcpy(&probe_length, &packet[PROBE_LEN],1);
                probeinfo_s.probe.resize(probe_length);
                memcpy((char*)probeinfo_s.probe.data(), &packet[PROBE_SSID],probe_length);
                if(channel_fix == false){
                    auto ret2 = probe_info.insert(make_pair(station_s,probeinfo_s));
                    exist_probereq = ret2.second;
                    try_add = true;
                }else if(channel_fix == true && int(packet[BEACON_LEN+essid_length+13]) == channel){
                    auto ret2 = probe_info.insert(make_pair(station_s,probeinfo_s));
                    exist_probereq = ret2.second;
                    try_add = true;
                }else{
                    try_add = false;
                }
                if(exist_probereq == true && try_add == true) { //if new key
                    memcpy(&probeinfo_s.bssidmac[0], &packet[BSSID], MAC_LEN);
                    memcpy(&probeinfo_s.pwr, &packet[PWR], 1);
                    memcpy(&probe_length, &packet[PROBE_LEN], 1);
                    probeinfo_s.probe.resize(probe_length);
                    memcpy((char*)probeinfo_s.probe.data(), &packet[PROBE_SSID],probe_length);
                    cnt_frame = 1;
                    probeinfo_s.frame = cnt_frame;
                    probe_info[station_s] = probeinfo_s;
                }else if(exist_probereq == false && try_add == true){ //if exist
                    pwr_probe = probe_info.find(station_s)->second.pwr;
                    essid_probe = probe_info.find(station_s)->second.probe;
                    cnt_frame = probe_info.find(station_s)->second.frame + 1; //frame count +1
                    probeinfo_s.pwr = pwr_probe;
                    probeinfo_s.probe = essid_probe;
                    probeinfo_s.frame = cnt_frame;
                    probe_info[station_s] = probeinfo_s;
                }
            }
            else if(cmp_proberes(&packet[TYPE])==1){ //PROBE RESPONSE
                memcpy(&station_s.mac_addr[0], &packet[ResSTATION], MAC_LEN);
                memcpy(&probeinfo_s.bssidmac[0], &packet[BSSID], MAC_LEN);
                memcpy(&probeinfo_s.pwr, &packet[PWR], 1);
                memcpy(&probe_length, &packet[0], 1); //none
                probeinfo_s.probe.resize(probe_length);
                memcpy((char*)probeinfo_s.probe.data(), &packet[62],probe_length);
                if(channel_fix == false){
                    auto ret3 = probe_info.insert(make_pair(station_s,probeinfo_s));
                    exist_proberes = ret3.second;
                    try_add = true;
                }else if(channel_fix == true && int(packet[BEACON_LEN+essid_length+13]) == channel){
                    auto ret3 = probe_info.insert(make_pair(station_s,probeinfo_s));
                    exist_proberes = ret3.second;
                    try_add = true;
                }else{
                    try_add = false;
                }
                if(exist_proberes == true && try_add == true) { //if new key
                    memcpy(&probeinfo_s.bssidmac[0], &packet[PROBE_SSID], MAC_LEN);
                    memcpy(&probeinfo_s.pwr, &packet[PWR], 1);
                    memcpy(&probe_length, &packet[0], 1); //none
                    probeinfo_s.probe.resize(probe_length);
                    memcpy((char*)probeinfo_s.probe.data(), &packet[PROBE_SSID], probe_length);
                    cnt_frame2 = 1;
                    probeinfo_s.frame = cnt_frame2;
                    probe_info[station_s] = probeinfo_s;
                }else if(exist_proberes == false && try_add == true){ //if exist
                    pwr_probe2 = probe_info.find(station_s)->second.pwr;
                    essid_probe2= probe_info.find(station_s)->second.probe;
                    cnt_frame2 = probe_info.find(station_s)->second.frame + 1; //frame count +1
                    probeinfo_s.pwr = pwr_probe2;
                    probeinfo_s.probe = essid_probe2;
                    probeinfo_s.frame = cnt_frame2;
                    probe_info[station_s] = probeinfo_s;
                }
            }

            //show beacon frame
            printf("no BSSID \t\t\t PWR\tCH\tBEACONS\tSSID\n");
            printf("=======================================================================================================\n");
            for(beacon_infoit = beacon_info.begin();beacon_infoit != beacon_info.end();beacon_infoit++){
                memcpy(char_beacon_bssid, &beacon_infoit->first, MAC_LEN);
                printf("%d ",i);
                print_mac(char_beacon_bssid);
                printf("%d\t",beacon_infoit->second.pwr);
                printf("%d\t",beacon_infoit->second.Chanel);
                printf("%d\t",beacon_infoit->second.beacons);
                printf("%s\n",beacon_infoit->second.essid.c_str());
                i++;
            }
            //show probe reqeust, probe response
            printf("\n");
            printf("no BSSID \t\t\t STATION\t\t\tPWR\tFRAMES\tPROBE\n");
            printf("=======================================================================================================\n");
            for(probe_infoit = probe_info.begin();probe_infoit != probe_info.end();probe_infoit++){
                memcpy(char_probe_station, &probe_infoit->first, MAC_LEN);
                memcpy(char_probe_bssid, &probe_infoit->second.bssidmac, MAC_LEN);
                printf("%d ",j);
                if(memcmp(&char_probe_bssid[0], &broad[0], MAC_LEN)==0){
                    printf("(not associated) \t : \t");
                }else{
                    print_mac(char_probe_bssid);
                }
                print_mac(char_probe_station);
                printf("%d\t",probe_infoit->second.pwr);
                printf("%d\t",probe_infoit->second.frame);
                printf("%s\n",probe_infoit->second.probe.c_str());
                j++;
            }
            printf("=======================================================================================================\n");

        }
        res = pcap_next_ex(handle, &header, &packet);
    }
    //Close
    map<mac_s, beaconinfo>().swap(beacon_info);
    map<mac_s, probeinfo>().swap(probe_info);
    pcap_close(handle);
    return 1;
}


