#include "header/header.h"
using namespace std;

int main(int argc, char *argv[])
{
    if (argc != 2 && argc != 4) {
      Usage(argv);
      return -1;
    }
    int channel = 0, c;
    char *dev = argv[1];
    while((c = getopt(argc, argv, "c:")) != -1)
    {
        switch(c)
        {
            case 'c':
                if(optarg) channel = atoi(optarg) ;
                break;
        }
    }
    show_airodump(dev, channel);
    return 0;
}
