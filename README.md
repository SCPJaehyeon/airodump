# airodump

Joongbu University CCIT - Network



#### Usage

```shell
ifconfig [device] down
iwconfig [device] mode monitor
ifconfig [device] up
./airodump [device] [-c channel]
ex:) ./airodump wlan0
ex:) ./airodump wlan0 -c 3
```
* If your device mode changes to Manage after a few seconds, perform the following command: 
service network-manager stop, (or) airmon-ng check kill


##### Picture
<img width="428" alt="1" src="https://user-images.githubusercontent.com/50411472/74128569-cbcb8a80-4c20-11ea-9e61-c46540de6178.PNG">


